﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    //Game Is played in 3 10 second gameplay loops
    //make basic decisions in 10 seconds

    //end of 10 seconds world ends 
    //game variables 
    int daysUntilEnd = 0;
    [SerializeField] TMP_Text timerText;
    float theTime = 10;
    public float speed = -1;
    bool playing = false;
    int choices1 = 0;
    int choices2 = 0;
    [SerializeField] TMP_Text textPrompt;
    [SerializeField] Text b1Text;
    [SerializeField] Text b2Text;
    [SerializeField] Text b3Text;
    [SerializeField] Text b4Text;

    //end message strings
    string action1;
    string action2;
    string action3;

    //naming character
    [SerializeField] InputField field;
    public string characterName;
    [SerializeField] TMP_Text titleOfGame;
    //objects of game scenes
    [SerializeField] GameObject nameCharacter;
    [SerializeField] GameObject mainGame;
    [SerializeField] GameObject endingView;
    //ending texts
    [SerializeField] TMP_Text action1Text;
    [SerializeField] TMP_Text action2Text;
    [SerializeField] TMP_Text action3Text;
    [SerializeField] TMP_Text finalText;
    void Update()
    {
        if(playing == true)
        {
        theTime += Time.deltaTime * speed;

       // string hours = Mathf.Floor((theTime % 216000) / 3600).ToString("00");
       // string minutes = Mathf.Floor((theTime % 3600)/60).ToString("00");
        string seconds = (theTime % 60).ToString("00");
       //timerText.text = hours + ":" + minutes + ":" + seconds;

        timerText.text = seconds;
            int x = 0;
            int.TryParse(seconds, out x);

            if ( x == 0)
                {
                LoadEnding();
                }
        }
    }

    //check decisions in round, change next round based on decisions

    //random ending unrelated to decisions... and text compilation from decisions for "story of player" 
    public void NameCharacter()
    {
        if (field.text != "")
        {
            characterName = field.text;
            nameCharacter.SetActive(false);
            mainGame.SetActive(true);
            titleOfGame.text = characterName + "'s" +" Story";
            ClickPlay();
        }
    }
    public void ClickPlay()
    {
        playing = true;
    }
    public void ClickStop()
    {
        playing = false;
    }
    public void ResetTime()
    {
        theTime = 10;
    }
    public void Option1()
    {
        if(choices2 == 0)
        {
        switch (choices1)
        {
            //play video games1
            case 0:
                LoadNextText("You played video games for hours. Now you want to...",
                    "Play more.", "Play more.", 
                    "Play more.", "Eat breakfast.");
                choices1 = 1; //play more
                action1 = characterName + " played videos games for hours.";
                break;
            //played more2
            case 1:
                LoadNextText("You played video games for hours. Now you want to...",
                    "Play and eat.", "Play and eat.",
                    "Play and eat.", "Play and eat.");
                choices1 = 5;
                    action2 = "Then they played more video games.";
                break;
            //went on walk
            case 2:
                LoadNextText("You went on a walk. Now you want to...",
                    "Eat healthy.", "Eat cheap.",
                    "Eat McDonalds.", "Work.");
                    choices1 = 7;
                    action2 = "Then they went on a walk.";
                    break;
            case 3:
                LoadNextText("You ate food with Charlie. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " ate food with Charlie.";
                break;
            case 4:
                LoadNextText("You ate food with Skyler. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " ate food with Skyler.";
                    break;
            case 5:
                    LoadNextText("You played video games and ate. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                    choices2 = 1;
                    action3 = "Finally, " + characterName + " played video games and ate.";
                    break;
            case 6:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 3;
                    action3 = "Finally, " + characterName + " went on a walk.";
                break;
                case 7:
                    LoadNextText("End of day...",
                        "End of day.", "End of day.",
                        "End of day.", "End of day.");
                    choices2 = 2;
                    action3 = "Finally, " + characterName + " ate a healthy meal.";
                    break;
            }
        }

    }
    public void Option2()
    {
        if (choices2 == 0)
        {
            switch (choices1)
        {
            //ate breakfast
            case 0:
                LoadNextText("You ate breakfast. Now you want to...",
                    "Go on walk.", "Work.",
                    "Meet Charlie.", "Meet Skyler.");
                choices1 = 2;
                action1 = characterName + " ate breakfast.";
                break;
            //played more2
            case 1:
                LoadNextText("You played video games for hours. Now you want to...",
                    "Play and eat.", "Play and eat.",
                    "Play and eat.", "Play and eat.");
                    choices1 = 5;
                    action2 = "Then they played more video games.";
                    break;
            //you worked
            case 2:
                LoadNextText("You worked. Now you want to...",
                    "Eat healthy.", "Eat cheap.",
                    "Eat McDonalds.", "Work more.");
                choices1 = 7;
                    action2 = "Then they worked.";
                break;
            case 3:
                LoadNextText("You ate Charlie. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " ate Charlie."; 
                break;
            case 4:
                LoadNextText("You ate Skyler. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " ate Skyler.";
                break;
            case 5:
                LoadNextText("You played video games and ate. End of day...",
                "End of day.", "End of day.",
                "End of day.", "End of day.");
                choices2 = 1;
                action3 = "Finally, " + characterName + " played video games and ate.";
                break;
            case 6:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 3;
                    action3 = "Finally, " + characterName + " worked.";
                break;
            case 7:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 2;
                    action3 = "Finally, " + characterName + " ate a cheap meal.";
                break;
            }
        }

    }
    public void Option3()
    {
        if (choices2 == 0)
        {
            switch (choices1)
        {
            case 0:
                LoadNextText("You meditated. Now you want to...",
                    "Go on walk.", "Work.",
                    "Meet Charlie.", "Meet Skyler.");
                choices1 = 2;
                action1 = characterName + " meditated.";
                break;
            //played more2
            case 1:
                LoadNextText("You played video games for hours. Now you want to...",
                    "Play and eat.", "Play and eat.",
                    "Play and eat.", "Play and eat.");
                    choices1 = 5;
                    action2 = "Then they played more video games.";
                    break;
            case 2:
                LoadNextText("You met with Charlie. Now you want to...",
                    "Eat with Charlie.", "Eat Charlie.",
                    "Go home alone.", "Go home with Charlie.");
                choices1 = 3;
                    action2 = "Then they met with Charlie.";
                break;
            case 3:
                LoadNextText("You went home alone. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " left their friend and went home.";
                break;
            case 4:
                LoadNextText("You went home alone. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " left their friend and went home.";
                    break;
            case 5:
                LoadNextText("You played video games and ate. End of day...",
                "End of day.", "End of day.",
                "End of day.", "End of day.");
                choices2 = 1;
                action3 = "Finally, " + characterName + " played video games and ate.";
                break;
            case 6:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 3;
                    action3 = "Finally, " + characterName + " played video games.";
                break;
            case 7:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 2;
                    action3 = "Finally, " + characterName + " ate McDonalds.";
                break;
            }
        }

    }
    public void Option4()
    {
        if (choices2 == 0)
        {
            switch (choices1)
        {
            case 0:
                LoadNextText("You went to the gym. Now you want to...",
                    "Go on walk.", "Work.",
                    "Meet Charlie.", "Meet Skyler.");
                choices1 = 2;
                action1 = characterName + " went to the gym.";
                break;
            //eat breakfast2
            case 1:
                LoadNextText("You ate breakfast. Now you want to...",
                    "Go on walk.", "Work.",
                    "Play video games.", "Play video games.");
                choices1 = 6;
                    action2 = "Then they ate breakfast.";
                break;
            case 2:
                LoadNextText("You met with Skyler. Now you want to...",
                    "Eat with Skyler.", "Eat Skyler.",
                    "Go home alone.", "Go home with Skyler.");
                choices1 = 4;
                    action2 = "Then they met with Skyler.";
                break;
            case 3:
                LoadNextText("You went home with Charlie. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " went home with Charlie.";
                    break;
            case 4:
                LoadNextText("You went home with Skyler. End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 4;
                    action3 = "Finally, " + characterName + " went home with Skyler.";
                    break;
            case 5:
                LoadNextText("You played video games and ate. End of day...",
                        "End of day.", "End of day.",
                        "End of day.", "End of day.");
                choices2 = 1;
                    action3 = "Finally, " + characterName + " played video games and ate.";
                    break;
            case 6:
                    LoadNextText("End of day...",
                        "End of day.", "End of day.",
                        "End of day.", "End of day.");
                    choices2 = 3;
                    action3 = "Finally, " + characterName + " played video games.";
                    break;
            case 7:
                LoadNextText("End of day...",
                    "End of day.", "End of day.",
                    "End of day.", "End of day.");
                choices2 = 2;
                    action3 = "Finally, " + characterName + " worked.";
                break;
            }
        }

    }

    public void LoadNextText(string prompt, string b1, string b2, string b3, string b4)
    {
        textPrompt.text = prompt;
        b1Text.text = b1;
        b2Text.text = b2;
        b3Text.text = b3;
        b4Text.text = b4;
    }
    public void LoadEnding()
    {
        action1Text.text = action1;
        action2Text.text = action2;
        action3Text.text = action3;
        finalText.text = "These were " + characterName + "'s final moments before the world ended."; 
        mainGame.SetActive(false);
        endingView.SetActive(true);
    }
    public void LoadCredits()
    { 
        SceneManager.LoadScene(2);
    }
    
}
