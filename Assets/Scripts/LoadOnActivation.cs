﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnActivation : MonoBehaviour
{

    private void Start()
    {
        if (gameObject.activeSelf == true)
        {
        SceneManager.LoadScene(2);
        }

    }

}
