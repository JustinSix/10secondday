﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuButtonClicks : MonoBehaviour
{
    public void Update()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        if (scene == 2)
        {
        if (Input.anyKey)
        {
                LoadMenu();
        }
        }

    }
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void PlayCredits()
    {
        SceneManager.LoadScene(2);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void OpenURLSubscribe()
    {
        Application.OpenURL("https://www.youtube.com/channel/UCTF55vLsfcuI2epJShoZa0Q?sub_confirmation=1");
    }
    
    public void LoadMenu()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene(0);
        }
        SceneManager.LoadScene(0);
    }
}
