﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadFirstScene : MonoBehaviour
{

    public void LoadScene()
    {
        print("working?");
        Invoke("LoadScene", 1f);
        SceneManager.LoadScene(1);
    }

}
